from tkinter import *
import sqlite3
import pandas as pd

'''
1. Decorator for delete, query and aubmit functions.
2. Fitr (parametrs should be taken from texbox) for query function.
3. Align the colums in query output.

'''

class Cars_database:
	def __init__(self):
		self.root = Tk()
		self.root.title('Car Database')
		#self.root.geometry("320x240")

		#Connect database (if it doesn't exist, create it) and create a Cursor
		self.conn = sqlite3.connect("cars.db")
		self.c = self.conn.cursor()

		'''#Create table
		self.c.execute("""CREATE TABLE addresses(
			company text, model text, engine volume real, hps integer)
			""")'''

		'''Create labels, textboxes and buttons'''

	#Labels

		self.l_company = Label(self.root, text="Company").grid(row=0, column=0)
		self.l_model = Label(self.root, text="Model").grid(row=1, column=0)
		self.l_engine = Label(self.root, text="Engine").grid(row=2, column=0)
		self.l_hps = Label(self.root, text="HPS").grid(row=3, column=0)
		#delete label
		self.del_label = Label(self.root, text='# ID').grid(row=5, column=0)
		#Query label (database's records will be shown here)
		self.query_label = Label(self.root, text='')
		self.query_label.grid(row=8, column=0, columnspan=3)

		#Textboxes
		self.e_company = Entry(self.root)
		self.e_model = Entry(self.root)
		self.e_engine = Entry(self.root)
		self.e_hps = Entry(self.root)
		#delete textbox
		self.e_del = Entry(self.root)
		#Query textbox (program will pick out records with parametrs given in this textbox)
		''' '''


		#Grid should be set separately, beacause if it would not, Entry was Nonetype object, and we couldn't do with it anything.
		self.e_company.grid(row=0, column=1)
		self.e_model.grid(row=1, column=1)
		self.e_engine.grid(row=2, column=1)
		self.e_hps.grid(row=3, column=1)
		#delete entry
		self.e_del.grid(row=5, column=1)

	#Buttons

		self.submit_button = Button(self.root, width=30, text='Add record to Database', command=self.submit)
		self.submit_button.grid(row=4, column=0, columnspan=2, pady=1, padx=10)

		self.delete_record = Button(self.root, width=30, text='Delete record', command=self.delete)
		self.delete_record.grid(row=6, column=0, columnspan=2, pady=1, padx=10)

		self.show_records = Button(self.root, width=30, text='Show records', command=self.query)
		self.show_records.grid(row=7, column=0, columnspan=2, pady=1, padx=10)
		'''
		self.quit_button = Button(self.root, width=15, text='Quit program', command=self.quit)
		self.quit_button.grid(row=8, column=0, columnspan=2, pady=1, padx=10)'''

	def delete(self):
		self.conn = sqlite3.connect('cars.db')
		self.c = self.conn.cursor()

		self.c.execute("DELETE FROM addresses WHERE oid = " + self.e_del.get())
		self.e_del.delete(0, END)

		self.conn.commit()
		self.conn.close()


	def submit(self):
		self.conn = sqlite3.connect('cars.db')
		self.c = self.conn.cursor()

		self.c.execute("INSERT INTO addresses VALUES(:e_company, :e_model, :e_engine, :e_hps)",
			{
				'e_company': self.e_company.get(),
				'e_model': self.e_model.get(),
				'e_engine': self.e_engine.get(),
				'e_hps': self.e_hps.get()
			})

		self.conn.commit()
		self.conn.close()

		#Clear entry fields
		self.e_company.delete(0, END),
		self.e_model.delete(0, END),
		self.e_engine.delete(0, END),
		self.e_hps.delete(0, END)


	def query(self):
		self.conn = sqlite3.connect('cars.db')
		self.c = self.conn.cursor()

		self.c.execute("SELECT *, oid FROM addresses")
		records = self.c.fetchall()
		
		#Create dataframe with records from database, which will be displayed in a label
		frame = pd.read_sql(sql='SELECT * FROM addresses', con=self.conn)
		self.query_label.destroy() #to avoid overwriting of labels
		self.query_label = Label(self.root, text=frame)
		self.query_label.grid(row=8, column=0, columnspan=2)

		self.conn.commit()
		self.conn.close()

	def run(self):
		self.root.mainloop()

	def quit(self):
		self.conn.commit()
		self.conn.close()

		self.root.quit()

if __name__ == "__main__":
	app = Cars_database()
	app.run()
